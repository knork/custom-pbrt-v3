#!/bin/bash


iterations=10

if [ -z $1 ]; then
iterations=10
else
iterations=$1
fi




declare -a branches=( "master" "ex_4_9" "slope" "slope+Original" "slope+tMin")
#declare -a branches=( "master" )

mkdir -p log



function bench {

	out=$(./cmake-build-release/pbrt $1) &>/dev/null
	echo "---------" >> $log
	echo "Scene: $2" >> $log
	echo "---------" >> $log

	echo "" >> $log
	out=$(echo "$out" | sed  '/Statistics\:/,$!d')
	echo "$out" >> $log
	echo "$2 -DONE"

}
mkdir -p cmake-build-release

for(( i=0;i<iterations; i++ ));do
    #TestType=$1 # For example Original
    DATE=`date +%Y-%m-%d-%H-%M-%S`
    log="log/bench_$DATE" # update log file for each iteration
    touch $log
    echo "" > $log


    for b in "${branches[@]}"
    do

        echo "##################" >> $log
        echo "Branch:$b" >> $log
        echo "##################" >> $log



        git checkout $b &>/dev/null
        cd cmake-build-release &>/dev/null

        echo "Building $b ..."
        cmake .. &>/dev/null
        make --silent -j8 &>/dev/null
         cd ..

        echo "Build $b complete"

#        bench  "../pbrt-v3-scenes/simple/teapot-metal.pbrt"  'SIMPLE'
        bench "../pbrt-v3-scenes/bathroom/bathroom.pbrt" 'bathroom example (32 Sobol Sampler, 600x380)'
        bench  "../pbrt-v3-scenes/dragon/f8-10.pbrt" 'glass drag example (64 Halton Sampler, 800x800)'
        bench  "../pbrt-v3-scenes/dragon/f14-5.pbrt" 'dual drag example (64 Halton Sampler, 900x540)'
        bench "../pbrt-v3-scenes/barcelona-pavilion/pavilion-day.pbrt" 'glass drag example (64 sobol Sampler, 800x425)'


    done
    ./compressBench.sh "$log"
done




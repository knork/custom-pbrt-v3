#!/bin/bash

benchFile=""
if [ -z $1 ]; then
benchFile="bench"
else
benchFile=$1
fi
DATE=`date +%Y-%m-%d-%H-%M-%S`
file="log/benchC_$DATE"

mkdir -p log
touch $file
echo "" >> $file
echo "$(date)" >> $file


out=$(cat $benchFile | egrep 'Scene:|Branch|Integrator::Render()|Ray-triangle intersection tests|Accelerator::Intersect()|^#')

out=$(echo "$out" |egrep "Branch|Scene" -A2 | sed "/#.*/d")

declare -A timing
branches="|X"
dots=""

b=0
col=0

while read -r line; do


    echo "$line"| grep -q "Branch"
    if  [ $? -eq 0 ] ; then
    some=$(echo "$line" | sed -e "s/Branch:\(.*\)/\1/")
    branches="$branches | $some "
    dots="$dots | ---"
    ((b++))
    col=0
    fi

    echo "$line"| grep -q "Scene"
    if  [ $? -eq 0 ] && [ -z "${timing[$col]}" ] ; then
    timing[$col]=$(echo "$line" | sed -e "s/Scene:\(.*\) example .*/\1/")
    fi

     echo "$line"| grep -q "Integrator"
    if  [ $? -eq 0 ] ; then
    some=$(echo "$line" | sed -e "s/Integrator::Render()\(.*\) (\(.*\))/\2/")

    ms=${some:11:13}
    ms=$(echo "ibase=10; $ms" | bc)
    s=${some:8:2}

    s=$(echo "ibase=10; $s" | bc)
    m=${some:5:2}
    m=$(echo "ibase=10; $m" | bc)
    h=${some:2:2}
    h=$(echo "ibase=10; $h" | bc)
    d=${some:0:1}
    d=$(echo "ibase=10; $d" | bc)


    let erg=0
    let "erg=  s + 60*m + 60*60*h+ 24*60*60*d"
    let "k=$ms/10"
    timing[$col]="${timing[$col]} | $erg.$k"
    timing[$col]="${timing[$col]}s"
    ((col++))
    fi




done <<< "$out"




echo "$branches" >> $file
echo "$dots" >> $file

for(( i=0;i<$col;i++ )) do
echo "${timing[$i]}" >> $file
done

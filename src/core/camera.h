
/*
    pbrt source code is Copyright(c) 1998-2016
                        Matt Pharr, Greg Humphreys, and Wenzel Jakob.

    This file is part of pbrt.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:

    - Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

    - Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
    TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

#if defined(_MSC_VER)
#define NOMINMAX
#pragma once
#endif

#ifndef PBRT_CORE_CAMERA_H
#define PBRT_CORE_CAMERA_H

// core/camera.h*
#include "pbrt.h"
#include "geometry.h"
#include "transform.h"
#include "film.h"
#include "sampling.h"

namespace pbrt {

    struct CameraSample {
        Point2f pFilm;
        Point2f pLens;
        Float time;
        Float random;
    };

    struct Bokeh {
        /// also used as type
        int blades = 0;
        /// strength and distribution of optical vignetting
        float radius = 0.5;
        float rateOfChange = 2;
        /// caches to avoid recalc
        float step;
        std::unique_ptr<Point2f[]> corners;
        /// Decribes the ratio of the two circle which determine the shape of the bokeh
        float innerRadius;
        /// 0 even destribution, <0 center is emphasized, >0 edges are emphasized
        int weightDistr;
        /// How strong the distribution is emphazised
        float weightStrength;
        float integral;
    };

    void initBokehVerts(Bokeh &bokehOptions);

// Camera Declarations
    class Camera {
    public:
        // Camera Interface
        Camera(const AnimatedTransform &CameraToWorld, Float shutterOpen,
               Float shutterClose, Film *film, const Medium *medium);

        virtual ~Camera();

        virtual Float GenerateRay(const CameraSample &sample, Ray *ray) const = 0;

        virtual Float GenerateRayDifferential(const CameraSample &sample,
                                              RayDifferential *rd) const;

        virtual Spectrum We(const Ray &ray, Point2f *pRaster2 = nullptr) const;

        virtual void Pdf_We(const Ray &ray, Float *pdfPos, Float *pdfDir) const;

        virtual Spectrum Sample_Wi(const Interaction &ref, const Point2f &u,
                                   Vector3f *wi, Float *pdf, Point2f *pRaster,
                                   VisibilityTester *vis) const;

        // Camera Public Data
        AnimatedTransform CameraToWorld;
        const Float shutterOpen, shutterClose;
        Film *film;
        const Medium *medium;


    protected:


        Bokeh bokehOptions;

        inline Point2f biasSample(const CameraSample &sample, float &weight) const {


            Point2f ret = sample.pLens;
            float edge = 0.0f;

            if (bokehOptions.blades >= 3) {

                Float a = sample.pLens.x;

                float xSample = sample.pLens.x;
                int index = static_cast<int>(xSample * bokehOptions.blades);
                index = std::min(index, bokehOptions.blades - 1);
                xSample = (xSample - (index * (1.0f / bokehOptions.blades))) * bokehOptions.blades;

                int previous = index + 1 == bokehOptions.blades ? 0 : index + 1;
                Point2f A = Point2f(0, 0);
                Point2f B = bokehOptions.corners[index];
                Point2f C = bokehOptions.corners[previous];


                Float r1 = xSample;
                Float r2 = sample.pLens.y;

                // edge and bias?

                ret = (1 - sqrt(r1)) * A + (sqrt(r1) * (1 - r2)) * B + (sqrt(r1) * r2) * C;
                edge = (r1);

            }// optical vignetting enabled
            else if (abs(bokehOptions.rateOfChange) > 0.002f) {

                float rateOfChange = bokehOptions.rateOfChange, radius = bokehOptions.innerRadius;

                Point2f s = ConcentricSampleDisk(sample.pLens);

                // convert film samples to [0-1] scale
                Point2f ndc = sample.pFilm;
                float res = std::max(film->fullResolution.x, film->fullResolution.y);
                ndc.y = film->fullResolution.y - ndc.y;
                ndc.y = ndc.y - film->fullResolution.y/2;
                ndc.x = ndc.x - film->fullResolution.x/2;
                ndc *=2;
                ndc.x /= res;
                ndc.y /= res;

                // convert to [-1,1]
                float c1 = s.x * s.x +  s.y *  s.y;

                s.x -= rateOfChange * ndc.x;
                s.y -= rateOfChange * ndc.y;

                float c2 = s.x * s.x +  s.y *  s.y;
                c2 *= radius; // shape


                // rejectionSampling
                if ( c2 > 1)
                    weight = -1.0e20f;

                ret = s;
                edge = std::max(c1, c2);


            } else { //default circle
                Point2f uOffset = 2.f * sample.pLens - Vector2f(1, 1);
                // Handle degeneracy at the origin
                if (uOffset.x == 0 && uOffset.y == 0) return Point2f(0, 0);

                // Apply concentric mapping to point
                Float theta, r;
                if (std::abs(uOffset.x) > std::abs(uOffset.y)) {
                    r = uOffset.x;
                    theta = PiOver4 * (uOffset.y / uOffset.x);
                } else {
                    r = uOffset.y;
                    theta = PiOver2 - PiOver4 * (uOffset.x / uOffset.y);
                }

                ret = r * Point2f(std::cos(theta), std::sin(theta));
                edge = std::abs(r);
            }


            float power = bokehOptions.weightStrength;
            float distr = bokehOptions.weightDistr;
            float intg = bokehOptions.integral;

            if (distr < 0) {
                edge= 1-edge;
                distr= -distr;
                power= -power;
            }

            float w = power * pow(edge, distr) + 1 - intg;

            if (weight > 0) {
                weight = w;
            }

            // default
            return ret;
        }

    };


    inline std::ostream &operator<<(std::ostream &os, const CameraSample &cs) {
        os << "[ pFilm: " << cs.pFilm << " , pLens: " << cs.pLens <<
           StringPrintf(", time %f ]", cs.time);
        return os;
    }

    class ProjectiveCamera : public Camera {
    public:
        // ProjectiveCamera Public Methods
        ProjectiveCamera(const AnimatedTransform &CameraToWorld,
                         const Transform &CameraToScreen,
                         const Bounds2f &screenWindow, Float shutterOpen,
                         Float shutterClose, Float lensr, Float focald, Film *film,
                         const Medium *medium)
                : Camera(CameraToWorld, shutterOpen, shutterClose, film, medium),
                  CameraToScreen(CameraToScreen) {
            // Initialize depth of field parameters
            lensRadius = lensr;
            focalDistance = focald;

            // Compute projective camera transformations

            // Compute projective camera screen transformations
            ScreenToRaster =
                    Scale(film->fullResolution.x, film->fullResolution.y, 1) *
                    Scale(1 / (screenWindow.pMax.x - screenWindow.pMin.x),
                          1 / (screenWindow.pMin.y - screenWindow.pMax.y), 1) *
                    Translate(Vector3f(-screenWindow.pMin.x, -screenWindow.pMax.y, 0));
            RasterToScreen = Inverse(ScreenToRaster);
            RasterToCamera = Inverse(CameraToScreen) * RasterToScreen;
        }

    protected:
        // ProjectiveCamera Protected Data
        Transform CameraToScreen, RasterToCamera;
        Transform ScreenToRaster, RasterToScreen;
        Float lensRadius, focalDistance;


    };
}
// namespace pbrt

#endif  // PBRT_CORE_CAMERA_H
